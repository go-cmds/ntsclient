# v0.8.0

* Correctly set default NTP server when NTS-KE FQDN has multiple A records
  (example: nts-test.strangled.net:443).
* Support server without a port, defaulting to NTS-KE port 4460.
* Add flag --count=n to query for time n times and then exit.
* Output Kiss-o'-death code when authenticator field is missing.
